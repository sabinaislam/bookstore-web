# Automate Bookstore Web 

## Prerequisite
- Java (test scripts ran using version: 1.8.0_241)
- Maven
- Git


## Environment Setup

- Clone this repository 
- chromedriver.exe version in this repository works with Chrome Browser Version 84.0.4147.105 (Official Build) (64-bit). 
- If you have different versions of Chrome browser, please download compatible version from https://chromedriver.chromium.org/downloads


## Test Execution

- Run 'mvn clean test' from command prompt or in preferable IDE. 
- Alternatively, you can also run as a TestNG suite from your preferred IDE. 

## Report

- Open index.html file from 'target\surefire-reports\html' in a browser 

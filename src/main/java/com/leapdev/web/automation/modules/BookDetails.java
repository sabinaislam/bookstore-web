package com.leapdev.web.automation.modules;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.leapdev.web.automation.objects.BookDetailsObjects;
import com.leapdev.web.automation.objects.BookListObjects;

public class BookDetails {
	
	public static void verifyBookDetails(WebDriver driver, String bookDetailsISBN, String bookDetailsTitle, String bookDetailsAuthor, String bookDetailsPublisher) throws InterruptedException {	
		Thread.sleep(1000);		
		int  actualbuttonSize = driver.findElements(By.xpath(BookDetailsObjects.backDetailsToBookStoreButton)).size();
		Assert.assertEquals(actualbuttonSize, 1);
		Assert.assertEquals(driver.findElement(By.xpath(BookDetailsObjects.bookDetailsISBN)).getText(), bookDetailsISBN);
		Assert.assertEquals(driver.findElement(By.xpath(BookDetailsObjects.bookDetailsTitle)).getText(), bookDetailsTitle);
		Assert.assertEquals(driver.findElement(By.xpath(BookDetailsObjects.bookDetailsAuthor)).getText(), bookDetailsAuthor);
		Assert.assertEquals(driver.findElement(By.xpath(BookDetailsObjects.bookDetailsPublisher)).getText(), bookDetailsPublisher);
		System.out.println("Book Details verified successfully.");
	} 

	public static void selectABook(WebDriver driver) throws InterruptedException {
		Thread.sleep(1000);	
		driver.findElement(By.xpath(BookListObjects.searchResultsBookTitle)).click();
	}
}

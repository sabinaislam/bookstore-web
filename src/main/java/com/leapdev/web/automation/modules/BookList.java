package com.leapdev.web.automation.modules;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import com.leapdev.web.automation.objects.BookListObjects;

public class BookList {
	
	public static void searchBooks(WebDriver driver, String searchText) throws InterruptedException {
		System.out.println("Searching with: " + searchText);
		driver.findElement(By.xpath(BookListObjects.searchField)).click();				
		driver.findElement(By.xpath(BookListObjects.searchField)).sendKeys(searchText);		
		Thread.sleep(1000);		
	} 
	
	public static void clearSearch(WebDriver driver) {		
		driver.findElement(By.xpath(BookListObjects.searchField)).clear();
	}
	
	public static void assertSearch(WebDriver driver, String searchText, String searchType) {
		if(searchType.equalsIgnoreCase("bookTitle")) {
			String searchResultsBookTitleValue = driver.findElement(By.xpath(BookListObjects.searchResultsBookTitle)).getText();
			Assert.assertEquals(searchResultsBookTitleValue, searchText, "Title mismatch");
		}		
		else if(searchType.equalsIgnoreCase("bookTitleDoesNottExist")) {
			String searchResultsNonExistingBookTitleValue = driver.findElement(By.xpath(BookListObjects.noSearchResultsFound)).getText();
			Assert.assertEquals(searchResultsNonExistingBookTitleValue, "No rows found", "Test failed");
		}		
		else if(searchType.equalsIgnoreCase("bookAuthor")) {
			String searchResultsAuthorValue = driver.findElement(By.xpath(BookListObjects.searchResultsAuthor)).getText();
			Assert.assertEquals(searchResultsAuthorValue, searchText, "Author mismatch");
		}		
		else if(searchType.equalsIgnoreCase("bookPublisher")) {
			String searchResultsPublisherValue = driver.findElement(By.xpath(BookListObjects.searchResultsPublisher)).getText();
			Assert.assertEquals(searchResultsPublisherValue, searchText, "Publisher mismatch");
		}		
		else if(searchType.equalsIgnoreCase("bookISBN")) {
			String searchResultsISBNValue = driver.findElement(By.xpath(BookListObjects.noSearchResultsFound)).getText();
			Assert.assertEquals(searchResultsISBNValue, "No rows found", "Test failed");
		}
		else 
			System.out.println("Searched with invalid parameter. Please update your test data.");

		System.out.println("Test executed and passed successfully.");
	}
}
package com.leapdev.web.automation.objects;

public class BookDetailsObjects {
	
	public static String backDetailsToBookStoreButton = "//button[@id='addNewRecordButton']";	
	public static String bookDetailsISBN = "//div[@id='ISBN-wrapper']//div[2]/label";
	public static String bookDetailsTitle = "//div[@id='title-wrapper']//div[2]";
	public static String bookDetailsAuthor = "//div[@id='author-wrapper']//div[2]";
	public static String bookDetailsPublisher = "//div[@id='publisher-wrapper']//div[2]";
}
package com.leapdev.web.automation.objects;

public class BookListObjects {

	public static String searchField = "//input[@placeholder ='Type to search']";	
	public static String searchButton = "//span[@id='basic-addon2']";	
	public static String searchResultsBookTitle = "//span[contains(@id, 'see-book')]/a";	
	public static String searchResultsAuthor = "//div[@class='rt-td'][3]";	
	public static String searchResultsPublisher = "//div[@class='rt-td'][4]";	
	public static String noSearchResultsFound = "//div[@class='rt-noData'][contains(text(), 'No rows found')]";	
}
package com.leapdev.web.automation.utility;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;


public class SeleniumFunctions {

	public static WebDriver driver;
	
	public WebDriver openBrowser(String browserName) {
		DesiredCapabilities capability = null;		
		capability = DesiredCapabilities.chrome();
		ChromeOptions options = new ChromeOptions();
		capability.setCapability(ChromeOptions.CAPABILITY, options);
		String driverfilePath = System.getProperty("user.dir") + "\\src\\main\\resources\\drivers\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", driverfilePath);
	    driver = new ChromeDriver(capability);
		return driver;
	}	
	
	public void getUrl(String url) {		
		driver.get(url);	
	}

	public void quitBrowser() {		
		driver.quit();		
	}
}
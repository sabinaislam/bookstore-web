package com.leapdev.web.automation.utility;

import org.testng.annotations.DataProvider;


public class TestDataProvider {
	
	@DataProvider(name="SearchBooks")   
	public static Object[][] getSearchParam(){
		return new Object[][] {
			{ "Git Pocket Guide", "bookTitle" },
			{ "9781449325862", "bookISBN" },
			{ "Kyle Simpson", "bookAuthor" },
			{ "O'Reilly Media", "bookPublisher" },
			{ "Non-existing Book Title", "bookTitleDoesNottExist" }
		}; 
	}
	
	@DataProvider(name="VerifyBookDetails")   
	public static Object[][] verifyBookDetails(){
		return new Object[][] {
			{ "9781491904244", "You Don't Know JS", "Kyle Simpson", "O'Reilly Media" }
		}; 
	}
}
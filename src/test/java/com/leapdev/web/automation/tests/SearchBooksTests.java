package com.leapdev.web.automation.tests;

import com.leapdev.web.automation.modules.BookDetails;
import com.leapdev.web.automation.modules.BookList;
import com.leapdev.web.automation.utility.SeleniumFunctions;
import com.leapdev.web.automation.utility.TestDataProvider;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;


public class SearchBooksTests {
	
	public static WebDriver browser; 
	public static SeleniumFunctions seleniumFunctions = new SeleniumFunctions();
	
	@BeforeClass()
	@Parameters({"baseURL"})
	public void setup(String baseURL) throws InterruptedException{
		browser = seleniumFunctions.openBrowser("Chrome");
		browser.get(baseURL);	
		Thread.sleep(1000);		
	}
	
	 @Test(dataProvider = "SearchBooks", dataProviderClass = TestDataProvider.class)
	 public void searchBooks(String searchText, String searchType) throws InterruptedException{
		 BookList.clearSearch(browser);
		 BookList.searchBooks(browser, searchText);
		 BookList.assertSearch(browser, searchText, searchType);
	 }
	 
	 @Test(dataProvider = "VerifyBookDetails", dataProviderClass = TestDataProvider.class)
	 public void verifyBookDetails(String bookDetailsISBN, String bookDetailsTitle, String bookDetailsAuthor, String bookDetailsPublisher ) throws InterruptedException{
		 BookList.clearSearch(browser);
		 BookList.searchBooks(browser, bookDetailsTitle);
		 BookDetails.selectABook(browser);
		 BookDetails.verifyBookDetails(browser, bookDetailsISBN, bookDetailsTitle, bookDetailsAuthor, bookDetailsPublisher);
	 }
	 
	@AfterClass()
	public void teardown() throws InterruptedException{
		seleniumFunctions.quitBrowser();
	}
}